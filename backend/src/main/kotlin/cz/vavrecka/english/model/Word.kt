package cz.vavrecka.english.model


data class Word(
    val czechTranslation: String,
    val englishTranslation: String,
    val phrases: List<Phrase> = emptyList(),
    val links: List<Link> = emptyList()
)
