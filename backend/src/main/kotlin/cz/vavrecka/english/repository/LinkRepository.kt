package cz.vavrecka.english.repository

import cz.vavrecka.english.domain.LinkRecord
import org.springframework.data.repository.CrudRepository
import java.util.UUID

interface LinkRepository : CrudRepository<LinkRecord, UUID> {

    fun findAllByWordId(wordId: UUID): List<LinkRecord>
}