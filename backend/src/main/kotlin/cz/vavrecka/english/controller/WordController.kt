package cz.vavrecka.english.controller

import cz.vavrecka.english.model.Word
import cz.vavrecka.english.service.WordService
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/v1/word")
class WordController(val wordService: WordService) {

    @PostMapping
    fun createWord(@RequestBody word: Word) = wordService.createWord(word)

    @GetMapping
    fun getAll(
        @RequestParam pageNumber: Int,
        @RequestParam pageSize: Int,
        @RequestParam direction: String = "DESC",
        @RequestParam sortProperty: String = "id"
    ) = wordService.getWords(PageRequest.of(pageNumber, pageSize, Sort.Direction.valueOf(direction), sortProperty))

}