package cz.vavrecka.english.domain

import org.springframework.data.annotation.Id
import org.springframework.data.annotation.PersistenceCreator
import org.springframework.data.annotation.Transient
import org.springframework.data.domain.Persistable
import org.springframework.data.relational.core.mapping.Table
import java.util.UUID

@Table("PHRASE")
class PhraseRecord(
    @JvmField @Id val id: UUID,
    val wordRecordId: UUID,
    val phrase: String,
    @Transient val new: Boolean = true
) :
    Persistable<UUID> {

    @PersistenceCreator
    constructor(id: UUID, wordRecordId: UUID, phrase: String) : this(id, wordRecordId, phrase, true)

    override fun getId() = id

    override fun isNew() = new
}