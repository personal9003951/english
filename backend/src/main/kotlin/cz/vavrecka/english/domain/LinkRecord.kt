package cz.vavrecka.english.domain

import org.springframework.data.annotation.Id
import org.springframework.data.annotation.PersistenceCreator
import org.springframework.data.annotation.Transient
import org.springframework.data.domain.Persistable
import org.springframework.data.relational.core.mapping.Table
import java.util.UUID


@Table("LINK")
data class LinkRecord(
    @JvmField @Id val id: UUID,
    val wordId: UUID,
    val link: String,
    @Transient val new: Boolean = true
) : Persistable<UUID> {

    @PersistenceCreator
    constructor(id: UUID, wordId: UUID, link: String) : this(id, wordId, link, true)

    override fun getId() = id

    override fun isNew() = new
}