package cz.vavrecka.english.service

import cz.vavrecka.english.domain.LinkRecord
import cz.vavrecka.english.domain.PhraseRecord
import cz.vavrecka.english.domain.WordRecord
import cz.vavrecka.english.model.FullWord
import cz.vavrecka.english.model.Word
import cz.vavrecka.english.repository.LinkRepository
import cz.vavrecka.english.repository.PhraseRepository
import cz.vavrecka.english.repository.WordRepository
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Service
import java.util.*

@Service
class WordService(
    val wordRepository: WordRepository,
    val phraseRepository: PhraseRepository,
    val linkRepository: LinkRepository
) {

    fun createWord(word: Word) {
        val wordId = UUID.randomUUID()

        wordRepository.save(
            WordRecord(
                id = wordId,
                czechTranslation = word.czechTranslation,
                englishTranslation = word.englishTranslation
            )
        )

        if (word.phrases.isNotEmpty()) {
            phraseRepository.saveAll(
                word.phrases.map {
                    PhraseRecord(id = UUID.randomUUID(), wordRecordId = wordId, it.phrase)
                }
            )
        }

        if (word.links.isNotEmpty()) {
            linkRepository.saveAll(word.links.map {
                LinkRecord(
                    id = UUID.randomUUID(),
                    wordId = wordId,
                    link = it.link
                )
            })
        }

    }

    fun getWords(pagination: PageRequest) = wordRepository.findAll(pagination)
        .map {
            FullWord(
                id = it.id,
                czechTranslation = it.czechTranslation,
                englishTranslation = it.englishTranslation,
                phrases = phraseRepository.findAllByWordRecordId(it.id),
                links = linkRepository.findAllByWordId(it.id)
            )
        }
}