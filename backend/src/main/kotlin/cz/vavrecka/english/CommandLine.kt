package cz.vavrecka.english

import cz.vavrecka.english.model.Link
import cz.vavrecka.english.model.Phrase
import cz.vavrecka.english.model.Word
import cz.vavrecka.english.service.WordService
import org.springframework.boot.CommandLineRunner
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class CommandLine(val wordService: WordService) : CommandLineRunner {


    @Transactional
    override fun run(vararg args: String?) {
        wordService.createWord(Word(czechTranslation = "auto", englishTranslation = "car"))
        wordService.createWord(
            Word(
                czechTranslation = "kočka",
                englishTranslation = "cat",
                listOf(Phrase("I find cat")),
                listOf(Link("ssssss"))
            )
        )
    }


}