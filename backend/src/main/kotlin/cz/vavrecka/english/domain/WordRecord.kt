package cz.vavrecka.english.domain

import org.springframework.data.annotation.Id
import org.springframework.data.annotation.PersistenceCreator
import org.springframework.data.annotation.Transient
import org.springframework.data.domain.Persistable
import org.springframework.data.relational.core.mapping.Table
import java.util.UUID


@Table("WORD")
data class WordRecord(
    @JvmField @Id val id: UUID,
    val czechTranslation: String,
    val englishTranslation: String,
    @Transient val new: Boolean = true
) : Persistable<UUID> {


    @PersistenceCreator
    constructor(
        id: UUID,
        czechTranslation: String,
        englishTranslation: String
    ) : this(id, czechTranslation, englishTranslation, true)


    override fun getId() = id

    override fun isNew() = new
}
