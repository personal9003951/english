package cz.vavrecka.english.model

import cz.vavrecka.english.domain.LinkRecord
import cz.vavrecka.english.domain.PhraseRecord
import java.util.*

class FullWord(
    val id: UUID,
    val czechTranslation: String,
    val englishTranslation: String,
    val phrases: List<PhraseRecord>,
    val links : List<LinkRecord>
)