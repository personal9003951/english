package cz.vavrecka.english.repository

import cz.vavrecka.english.domain.WordRecord
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.PagingAndSortingRepository
import java.util.*

interface WordRepository : PagingAndSortingRepository<WordRecord, UUID>, CrudRepository<WordRecord, UUID> {
}