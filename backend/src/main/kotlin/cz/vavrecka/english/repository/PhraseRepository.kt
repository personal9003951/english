package cz.vavrecka.english.repository

import cz.vavrecka.english.domain.PhraseRecord
import org.springframework.data.repository.CrudRepository
import java.util.UUID

interface PhraseRepository : CrudRepository<PhraseRecord, UUID> {

    fun findAllByWordRecordId(wordRecordId: UUID): List<PhraseRecord>
}